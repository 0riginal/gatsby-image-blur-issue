import * as React from 'react';
import Img, { FluidObject } from 'gatsby-image';
import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

export default function () {
  const sourceImages = useArtDirectionImages();

  const [imageIndex, setImageIndex] = React.useState(0);
  const imagesToDisplay = sourceImages.slice(imageIndex, imageIndex + 3);

  const onNextClick = () => {
    if (imageIndex + 2 < sourceImages.length) {
      setImageIndex(imageIndex + 3);
    }
  }

  const onPrevClick = () => {
    if (imageIndex > 0) {
      setImageIndex(imageIndex - 3);
    }
  }

  return (
    <Main>
      <ImageGallery>
        {imagesToDisplay.map(({ fluid }: { fluid: FluidObject }, index: number) =>
          <Img key={index} fluid={fluid}></Img>
        )}
      </ImageGallery>
      <ButtonBar>
        <button onClick={onPrevClick}>Previous</button>
        <button onClick={onNextClick}>Next</button>
      </ButtonBar>
    </Main>
  );
};

const useArtDirectionImages = () => {
  const {
    allImageSharp: { nodes: images }
  } = useStaticQuery(
    graphql`
      query {
        allImageSharp {          
          nodes {            
            fluid(maxHeight: 203, maxWidth: 360) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }          
        }
      }
    `
  );

  return images;
};

const Main = styled.main`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 2;  
`;

const ImageGallery = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(360px, 1fr));
  grid-template-rows: 1;
  grid-row: 1;
  grid-column: 0 2;    
`;

const ButtonBar = styled.div`
  display: flex;
  justify-content: center;
  grid-colum: 0;
  grid-row: 2;
`;